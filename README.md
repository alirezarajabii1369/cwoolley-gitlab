# Chad Woolley's GitLab README

## Connect with me

<a href="https://www.linkedin.com/in/thewoolleyman/"><img align="left" src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=for-the-badge&logo=LinkedIn&logoColor=white" /></a>
<a href="mailto:cwoolley@gitlab.com"><img align="left" src="https://img.shields.io/badge/Email-EA4335?&style=for-the-badge&logo=Gmail&logoColor=white" /></a>
<a href="https://calendly.com/cwoolley-gitlab/15min"><img align="left" src="https://img.shields.io/badge/Schedule a Meeting-purple?&style=for-the-badge&logo=Google Calendar&logoColor=white" /></a>
<a href="https://gitlab.zoom.us/my/gitlab.chadwoolley"><img align="left" src="https://img.shields.io/badge/Zoom-brightgreen?&style=for-the-badge&logo=Google Calendar&logoColor=white" /></a>
&nbsp;

## Useful Links

* Notes/Instructions on how I set up my macOS development machine: [GitLab Workstation Setup Notes](https://gitlab.com/cwoolley-gitlab/cwoolley-gitlab/-/blob/main/gitlab-workstation-setup-notes.md)
* Developer Cheatsheet which has evolved from some of my onboarding notes: [GitLab Developer Cheatsheet](https://about.gitlab.com/handbook/engineering/development/dev/create/editor/developer-cheatsheet/)
* How I manage my JetBrains RubyMine config in source control: [https://gitlab.com/jetbrains-ide-config/jetbrains-ide-config-gitlab](https://gitlab.com/jetbrains-ide-config/jetbrains-ide-config-gitlab)

## About Me

I've been a professional full-stack polyglot coder/sysadmin for 30+ years, from mainframes to cloud computing. I've been primarily a full-stack web developer, since the dawn of the web in the early 1990's.

Currently, I'm a Senior Engineer on the [GitLab Create::Editor team](https://about.gitlab.com/handbook/engineering/development/dev/create-editor/), since November 2019 (I was hired as and still work as a full-stack engineer, but am currently classified as backend due to internal transfers).

Here's my [interactive resume/CV](https://resume.thewoolleyweb.com/), which is a [SPA](https://en.wikipedia.org/wiki/Single-page_application) I created in Vue.js to helped me get my job at GitLab (and hasn't been updated since then). 

My personal ID is `thewoolleyman` everywhere - feel free to connect, but all opinions expressed elsewhere are mine only (and usually unfiltered), and do not represent GitLab.
